
package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> items = new ArrayList<>();
    public Fridge(){
        items = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if((totalSize()-nItemsInFridge())>0){
            items.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() > 0) {
            if(items.contains(item)){
                items.remove(item);
            }
            else{
                throw new NoSuchElementException();
            }
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++){
            FridgeItem item = items.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredFood){
            items.remove(expiredItem);
            }
        return expiredFood;
    }
    
}
